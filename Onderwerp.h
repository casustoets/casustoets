/*
 * Onderwerp.h
 *
 *  Created on: 21 okt. 2016
 *      Author: niels
 */

#ifndef ONDERWERP_H_
#define ONDERWERP_H_

#include <vector>
#include "Vraag.h"

class Onderwerp{
public:
	Onderwerp();
	Onderwerp(std::string onderwerpNaam);
	~Onderwerp();

	const std::string& getOnderwerpNaam() const;

	void add(const Vraag& vraag);

private:
	std::vector<Vraag*> vragen;
	const std::string onderwerpNaam;
};

#endif /* ONDERWERP_H_ */


