/*
 * Onderwerp.cpp
 *
 *  Created on: 21 okt. 2016
 *      Author: niels
 */

#include "Onderwerp.h"
#include "Vraag.h"

Onderwerp::Onderwerp()
:onderwerpNaam()
{
}

Onderwerp::Onderwerp(std::string onderwerpNaam)
:onderwerpNaam(onderwerpNaam)
{
}

Onderwerp::~Onderwerp()
{
}

const std::string& Onderwerp::getOnderwerpNaam() const
{
	return onderwerpNaam;
}

void Onderwerp::add(const Vraag& vraag)
{
	vragen.push_back(vraag);
}
