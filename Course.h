/*
 * Course.h
 *
 *  Created on: 21 okt. 2016
 *      Author: niels
 */

#ifndef COURSE_H_
#define COURSE_H_

#include <vector>
#include "Onderwerp.h"

class Course{
public:
	Course();
	Course(std::string courseNaam);
	~Course();

	const std::string& getCourseNaam() const;

	void add(Onderwerp onderwerp);

private:
	std::vector<Onderwerp> onderwerpen;
	const std::string courseNaam;
};

#endif /* COURSE_H_ */

