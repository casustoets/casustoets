/*
 * OpenVraag.cpp
 *
 *  Created on: 23 okt. 2016
 *      Author: niels
 */

#include "OpenVraag.h"
#include "UserInterface.h"
#include <string>

OpenVraag::OpenVraag()
:Vraag(), woordenlijst()
{
}

OpenVraag::OpenVraag(unsigned short int aantalPunten,
		std::string vraag,
		std::vector<std::string> woordenlijst)
:Vraag(vraag,aantalPunten), woordenlijst(woordenlijst)
{
}

OpenVraag::~OpenVraag()
{
}

short OpenVraag::beantwoordVraag() {
	std::vector<std::string> strings = UserInterface::stelVraag(vraag);
	float punten = aantalPunten / woordenlijst.size();
	float score = 0;
	for(std::string woord : woordenlijst){
		for(std::string antwoordwoord : strings){
			if(antwoordwoord == woord)
				score +=punten;
		}
	}
	std::cout << score;
	return score;
}
