/*
 * MeerkeuzeVraag.h
 *
 *  Created on: 23 okt. 2016
 *      Author: niels
 */

#ifndef MEERKEUZEVRAAG_H_
#define MEERKEUZEVRAAG_H_

#include <string>
#include <vector>

#include "Vraag.h"

class MeerkeuzeVraag: public Vraag{
public:
	MeerkeuzeVraag();
	MeerkeuzeVraag(unsigned short int aantalPunten, std::string vraag,std::vector<std::string> antwoorden,std::string correcteAntwoord);
	~MeerkeuzeVraag();
	short beantwoordVraag(const std::string&  antwoord);
private:
	std::vector<std::string> antwoorden;
	std::string correcteAntwoord;
};



#endif /* MEERKEUZEVRAAG_H_ */
