/*
 * Gebruiker.h
 *
 *  Created on: Oct 19, 2016
 *      Author: user
 */

#ifndef GEBRUIKER_H_
#define GEBRUIKER_H_

#include <string>
#include <vector>
#include "Vraag.h"

class Gebruiker {
public:
	Gebruiker();
	Gebruiker(std::string username, std::string password);
	virtual ~Gebruiker();

	const std::string& getUsername() const {
		return username;
	}

private:
	std::string username;
	std::string password;
	std::vector<Vraag> beantwoordeVragen;
};

#endif /* GEBRUIKER_H_ */
