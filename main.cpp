
/*
 * main.cpp
 *
 *  Created on: 19 okt. 2016
 *      Author: niels
 */

#include <iostream>
#include <string>
#include <vector>

#include "Gebruiker.h"
#include "Toets.h"

#include "Semester.h"
#include "Onderwerp.h"
#include "Course.h"
#include "Vraag.h"
#include "MeerkeuzeVraag.h"
#include "OpenVraag.h"


#include "UserInterface.h"
#include "ToetsApp.h"



int main(){
	OpenVraag testvraag(8,"open vraag?",{"ok"});
	Onderwerp spek("Spek");
	spek.add(testvraag);
	testvraag.beantwoordVraag();
	ToetsApp toetsapp;
	toetsapp.displayMenu();
	return 0;
}

//OUDE CODE POTEN AF
//VIES
//BAH
/*
 *

#include <iostream>
#include <string>
#include <vector>

#include "Gebruiker.h"
#include "Toets.h"

#include "Semester.h"
#include "Onderwerp.h"
#include "Course.h"
#include "Vraag.h"
#include "MeerkeuzeVraag.h"
#include "OpenVraag.h"

std::vector<Semester> semesterLijst;

void genereerVragen(){
	Semester semester1("Semester_1");
	Semester semester2("Semester_2");

	semesterLijst.push_back(semester1);
	semesterLijst.push_back(semester2);

	Course aardrijkskunde("Aardrijkskunde");
	Course geschiedenis("Geschiedenis");

	semester1.add(aardrijkskunde);
	semester2.add(aardrijkskunde);
	semester2.add(geschiedenis);

	Onderwerp hilo("Historische locaties");

	geschiedenis.add(hilo);aardrijkskunde.add(hilo);

	MeerkeuzeVraag m1(10,"vraag?",{"A","B","C"},"A");
	hilo.add(m1);
	MeerkeuzeVraag m2(10,"vraag?",{"A","B","C"},"A");
	hilo.add(m2);
	MeerkeuzeVraag m3(10,"vraag?",{"A","B","C"},"A");
	hilo.add(m3);
	MeerkeuzeVraag m4(10,"vraag?",{"A","B","C"},"A");
	hilo.add(m4);
	MeerkeuzeVraag m5(10,"vraag?",{"A","B","C"},"A");
	hilo.add(m5);

	OpenVraag o1(10,"vraag?",{"42"});
	hilo.add(o1);
	OpenVraag o2(10,"vraag?",{"42"});
	hilo.add(o2);
	OpenVraag o3(10,"vraag?",{"42"});
	hilo.add(o3);
	OpenVraag o4(10,"vraag?",{"42"});
	hilo.add(o4);
	OpenVraag o5(10,"vraag?",{"42"});
	hilo.add(o5);

}

Gebruiker accountAanmaken(){
	std::string username;
	std::string password;

	std::cout << "Voer een gebruikersnaam in:" << std::endl;
	std::cin >> username;
	std::cout << "Voer een wachtwoord in:" << std::endl;
	std::cin >> password;
	std::cout << username << " " << password << std::endl;

	Gebruiker tempGebruiker = Gebruiker(username,password);
	return tempGebruiker;
}

int main(){


	std::string input;
	Gebruiker gebruiker;
	Toets toets;
	bool done = false;

	genereerVragen();

	std::cout << "Welkom in de toets applicatie" << std::endl;
	std::cout << "Om een account aan te maken typ 'account'" << std::endl;
	std::cout << "Om een toets te maken typ 'toets'" << std::endl;

	while(!done){

		std::cout << "Voer uw keuze in'" << std::endl;
		std::cin >> input;

		if(input == "account")
		{
			gebruiker = accountAanmaken();
		}
		else if(input == "toets")
		{
			toets.kiesToets(semesterLijst);

			done = true;
		}
		else
		{
			std::cerr << "Not correct input" << std::endl;
		}

		input = "";
	}
	return 0;
}


 */
