/*
 * Toets.cpp
 *
 *  Created on: 23 okt. 2016
 *      Author: niels
 */

#include "Toets.h"

Toets::Toets()
{
}

Toets::~Toets()
{
}

void Toets::startToets()
{
	return;
}

void Toets::kiesToets(const std::vector<Semester>& semesterlijst)
{
	std::string input;
	bool done = false;
	unsigned short semesternummer;

	while(!done)
	{
		std::cout << "Kies een semester:" << std::endl;
		for(Semester semesterItem:semesterlijst){
			std::cout << semesterItem.getSemesterNaam() << std::endl;
		}

		std::cin >> input;

		for(unsigned short i = 0;i<semesterlijst.size();i++){
			if(semesterlijst[i].getSemesterNaam() == input)
			{
				semesternummer = i;

				done = true;
			}
		}
	}

	done = false;
	unsigned short coursenummer = 0;

	unsigned long tempSize = semesterlijst[0].getSize();

	while(!done)
		{
			std::cout << "Kies een course:" << std::endl;
			std::cout << tempSize << std::endl;

			for(unsigned short i = 0; i < tempSize;i++){
				std::cout << semesterlijst[semesternummer].getCourses()[i].getCourseNaam() << "test" <<std::endl;
			}

			std::cin >> input;

			for(unsigned short i = 0; i < semesterlijst[semesternummer].getCourses().size();i++){
				std::cout << input << std::endl;
				if(semesterlijst[semesternummer].getCourses()[i].getCourseNaam() == input)
				{
					coursenummer = i;

					done = true;
				}
			}
		}

	startToets();
}
