/*
 * UserInterface.cpp
 *
 *  Created on: Nov 1, 2016
 *      Author: user
 */

#include "UserInterface.h"

UserInterface::UserInterface() {
	// TODO Auto-generated constructor stub

}

UserInterface::~UserInterface() {
	// TODO Auto-generated destructor stub
}

std::vector<std::string> UserInterface::stelVraag(std::string& question) {
	std::cout << question << std::endl;
	bool done = false;
	std::vector<std::string> input;
	while(!done){
				std::cout << "Typ een antwoord in en druk op enter:" << std::endl;
				/*
				 * dit werkt nog niet zoals het hoort
				 */
				std::string temp;
				std::cin >> temp;
				input.push_back(temp);

				if(input[0] != "")
					{
					done = true;
					}
				else
					{
					std::cerr << "Not correct input." << std::endl;
					}
				}
	return input;
}

std::string UserInterface::stelMCVraag(std::string question,
		std::vector<std::string> answers) {
	std::cout << question << std::endl;
	for(std::string answer : answers){
		std::cout << answer << std::endl;
	}
	bool done = false;
	std::string input = "";
	while(!done){
				std::cout << "Typ een antwoord in en druk op enter:" << std::endl;
				std::cin >> input;

				if(std::find(answers.begin(), answers.end(), input) != answers.end() )
					{
					done = true;
					}
				else
					{
					std::cerr << "Not correct input." << std::endl;
					}
				}
	return input;
}
