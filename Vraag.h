/*
 * Vraag.h
 *
 *  Created on: 23 okt. 2016
 *      Author: niels
 */

#ifndef VRAAG_H_
#define VRAAG_H_

#include <string>

class Vraag{
public:
	Vraag();
	Vraag(std::string vraag, unsigned short int aantalPunten);
	virtual ~Vraag();
	virtual short beantwoordVraag()=0;
protected:
	std::string vraag;
	unsigned short int aantalPunten;
};

#endif /* VRAAG_H_ */
