/*
 * Semester.cpp
 *
 *  Created on: 21 okt. 2016
 *      Author: niels
 */

#include "Semester.h"
#include <iostream>

Semester::Semester()
:semesterNaam()
{
}

Semester::Semester(std::string semesterNaam)
:semesterNaam(semesterNaam)
{
}

Semester::~Semester()
{
}

const std::string& Semester::getSemesterNaam() const
{
	return semesterNaam;
}

void Semester::add(Course course)
{
	courses.push_back(course);
}

const std::vector<Course>& Semester::getCourses() const {
	return courses;
	std::cout << __PRETTY_FUNCTION__ << "test";
}

unsigned long int Semester::getSize() const
{
	return courses.size();
}

Course Semester::getCourse(unsigned long i){
	return courses.at(i);
}
