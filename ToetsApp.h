/*
 * ToetsApp.h
 *
 *  Created on: Nov 2, 2016
 *      Author: user
 */

#ifndef TOETSAPP_H_
#define TOETSAPP_H_

#include "UserInterface.h"
#include "Gebruiker.h"
#include <vector>
class ToetsApp {
public:
	ToetsApp();
	virtual ~ToetsApp();
	void displayMenu();

private:
	Gebruiker* currentUser = new Gebruiker;
	std::vector<Gebruiker> gebruikerVector;
	void registerUser();
	void loginUser();
	void createToets();

};

#endif /* TOETSAPP_H_ */
