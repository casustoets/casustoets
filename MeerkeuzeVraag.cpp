/*
 * MeerkeuzeVraag.cpp
 *
 *  Created on: 23 okt. 2016
 *      Author: niels
 */

#include "MeerkeuzeVraag.h"

MeerkeuzeVraag::MeerkeuzeVraag()
:antwoorden(), correcteAntwoord()
{
}

MeerkeuzeVraag::MeerkeuzeVraag(unsigned short int aantalPunten, std::string vraag,
		std::vector<std::string> antwoorden,
		std::string correcteAntwoord)
:Vraag(vraag,aantalPunten), antwoorden(antwoorden), correcteAntwoord(correcteAntwoord)
{
}

MeerkeuzeVraag::~MeerkeuzeVraag()
{
}

short MeerkeuzeVraag::beantwoordVraag(const std::string& antwoord) {
	if(antwoord == correcteAntwoord){
		return aantalPunten;
	}
	return 0;
}
