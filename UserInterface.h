/*
 * UserInterface.h
 *
 *  Created on: Nov 1, 2016
 *      Author: user
 */

#ifndef USERINTERFACE_H_
#define USERINTERFACE_H_
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
class UserInterface {
public:
	UserInterface();
	virtual ~UserInterface();
	static std::vector<std::string> stelVraag(std::string& question);
	static std::string stelMCVraag(std::string question, std::vector<std::string> answers);
};

#endif /* USERINTERFACE_H_ */
