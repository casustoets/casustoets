/*
 * Semester.h
 *
 *  Created on: 21 okt. 2016
 *      Author: niels
 */

#ifndef SEMESTER_H_
#define SEMESTER_H_

#include <vector>

#include "Course.h"

class Semester{
public:
	Semester();
	Semester(std::string semesterNaam);
	~Semester();

	const std::string& getSemesterNaam() const;

	void add(Course course);

	unsigned long int getSize() const;

	const std::vector<Course>& getCourses() const;

	Course getCourse(unsigned long i);

private:
	std::vector<Course> courses;
	const std::string semesterNaam;
};

#endif /* SEMESTER_H_ */
