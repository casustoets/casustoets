/*
 * OpenVraag.h
 *
 *  Created on: 23 okt. 2016
 *      Author: niels
 */

#ifndef OPENVRAAG_H_
#define OPENVRAAG_H_

#include <vector>
#include <string>

#include "Vraag.h"

class OpenVraag: public Vraag{
public:
	OpenVraag();
	OpenVraag(unsigned short int aantalPunten,std::string vraag,std::vector<std::string> woordenlijst);
	~OpenVraag();
	short beantwoordVraag();

private:
	std::vector<std::string> woordenlijst;
};



#endif /* OPENVRAAG_H_ */
