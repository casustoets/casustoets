/*
 * Vraag.cpp
 *
 *  Created on: 23 okt. 2016
 *      Author: niels
 */

#include "Vraag.h"

Vraag::Vraag()
:vraag(""), aantalPunten(0)
{
}

Vraag::Vraag(std::string vraag, unsigned short int aantalPunten)
:vraag(vraag), aantalPunten(aantalPunten)
{
}

Vraag::~Vraag()
{
}
