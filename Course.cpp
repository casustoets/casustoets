/*
 * Course.cpp
 *
 *  Created on: 21 okt. 2016
 *      Author: niels
 */

#include "Course.h"

Course::Course()
:courseNaam()
{
}

Course::Course(std::string courseNaam)
:courseNaam(courseNaam)
{
}

Course::~Course()
{
}

const std::string& Course::getCourseNaam() const
{
	return courseNaam;
}

//this is where the magic happens
void Course::add(Onderwerp onderwerp)
{
}
