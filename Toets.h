/*
 * Toets.h
 *
 *  Created on: 23 okt. 2016
 *      Author: niels
 */

#ifndef TOETS_H_
#define TOETS_H_

#include "Vraag.h"
#include "Semester.h"
#include "Course.h"

#include <iostream>
#include <string>
#include <vector>

class Toets{
public:
	Toets();
	~Toets();

	void startToets();
	void kiesToets(const std::vector<Semester>& semesterlijst);

private:
	unsigned short int typeToets;
	std::vector<Vraag> toetsVragen;
};



#endif /* TOETS_H_ */
